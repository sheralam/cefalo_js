package org.sh.user;

import java.util.Comparator;

/**
 * Created by sheralam on 5/12/17.
 */
public interface User  {
    public String getName();
    public int getId();
    public int getType();
}
