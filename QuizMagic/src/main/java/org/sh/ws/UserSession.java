package org.sh.ws;

import org.sh.user.User;

import javax.websocket.Session;

/**
 * Created by sheralam on 5/12/17.
 */
public class UserSession {
    public User getUser() {
        return user;
    }

    public  UserSession(Session pSession,User pUser){
        session = pSession;
        user = pUser;
    }

    public Session getSession() {
        return session;
    }

    private Session session;
    private User user;


}
