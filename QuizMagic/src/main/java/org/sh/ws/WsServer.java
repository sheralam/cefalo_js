package org.sh.ws;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.sh.user.Host;
import org.sh.user.Reciever;
import org.sh.user.User;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;


@ServerEndpoint("/websocketendpoint/{quizId}")
public class WsServer {

    private static int HostCount = 0;
    private static Map<String, UserSession> peers = new HashMap<>();
    private static Map<Integer, String> peersMap = new HashMap<>();


    @OnOpen
    public void onOpen(@PathParam("quizId") String quizId, Session session) {
        User user;
        if (HostCount >= 1) {
            user = new Reciever(("Reciever " + HostCount), HostCount, 1);
        } else {
            user = new Host(("Host " + HostCount), HostCount, 0);
        }
        peersMap.put(user.getId(), session.getId());
        peers.put(session.getId(), new UserSession(session, user));
/*        if (HostCount >= 1) {
            sendMsgToHost(user.getName() + " is connected !!");
        }*/
        HostCount++;

    }

    @OnClose
    public void onClose(Session session) {
        UserSession userSession = peers.remove(session.getId());
        peersMap.remove(userSession.getUser().getId());
        if (userSession.getUser().getType() == 0) {
            clearUp();
        }else {
            String msg = "{\"redirectClient\":\"0\",\"msg\":{\"msgType\":\"receiverClosed\"}}";
            sendMsgToHost(msg);
        }
    }

    @OnMessage
    public void onMessage(String message) {
        int redirectClient =getRedirectClient(message);
        if(redirectClient == 1000){
            //send to all but host
            sendMsgToRecievers(message);
        }else if(redirectClient>=1){
            // send msg to specific
            sendMsgToReciever(redirectClient,message);
        } else if(redirectClient < 0) {
            //  send msg to all
            sendMsgToAll(message);
        }else {
            // send to host
            sendMsgToHost(message);
        }

    }

    private int getRedirectClient(String jsonStr) {
        JSONParser parser = new JSONParser();
        int redirectClient=0;
        try {
            Object obj = parser.parse(jsonStr);
            JSONObject jsonObject = (JSONObject) obj;
            redirectClient = Integer.valueOf((String) jsonObject.get("redirectClient"));
        } catch (ParseException e) {
            System.out.println("E : Message parsing string:"+jsonStr);
            System.out.println("Exception :"+e.getLocalizedMessage());
        }

        return redirectClient;
    }


    private void sendMsgToHost(String message) {
        try {
            Thread.sleep(Math.round(Math.random() * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            peers.get(peersMap.get(0)).getSession().getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMsgToReciever(int redirectClient , String message) {
        try {
            peers.get(peersMap.get(redirectClient)).getSession().getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMsgToRecievers(String message) {

        Iterator<String> iterator = peers.keySet().iterator();
        while (iterator.hasNext()) {
            try {
                UserSession userSession = peers.get(iterator.next());
                if (userSession.getUser().getType() != 0) {
                    userSession.getSession().getBasicRemote().sendText(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void sendMsgToAll(String message) {

        Iterator<String> iterator = peers.keySet().iterator();
        while (iterator.hasNext()) {
            try {
                peers.get(iterator.next()).getSession().getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @OnError
    public void onError(Throwable e) {
        clearUp();
        e.printStackTrace();
    }

    private void clearUp() {
        HostCount = 0;
        peersMap.clear();
        peers.clear();

    }


}
