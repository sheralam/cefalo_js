/**
 * Created by sheralam on 5/13/17.
 */

var quesModules = quesModules || {};

(function (module) {
    var questionString = {
        "title": "Quiz Magic Cefalo",
        "questions": [
            {
                "question": "What is your name ?",
                "answers": [
                    "Rajib",
                    "Sajib",
                    "Mujib",
                    "Zujib"
                ],
                "correctAnswer": 0
            }
            ,
            {
                "question": "What is you country name",
                "answers": [
                    "India",
                    "Bangladesh",
                    "Pakistan",
                    "England"
                ],
                "correctAnswer": 1
            }
            ,
            {
                "question": "Who is  #1 ODI Batsman",
                "answers": [
                    "AB de Villiers",
                    "David Warner",
                    "Virat Kohli",
                    "Joe Root"
                ],
                "correctAnswer": 0
            }
        ]
    };
    module.questionSet = questionString;

})(quesModules);






