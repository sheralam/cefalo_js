/**
 * Created by sheralam on 5/14/17.
 */
function init(webSocket) {

    var index = 0;
    var numberOfQuestion;
    /*  Private Function*/
    var numberOfClient = 0, tmpNumberOfClient = 0;

    /*{"redirectClient":"2","msg":{}}*/


    function showWaitingMsg() {

        $("#showParticipants h1").text(quesModules.questionSet.title);
        $("#showParticipants h3").text("Waiting For participant to Join ..");
    }


    /* Public functions*/
    var started = false;
    function hostStart() {
        $('.host #participantForm').hide();
        $('.host #showQuestion').show();
        numberOfQuestion = quesModules.questionSet.questions.length;
        started = true;
        showQuestion();

    }

    /*
     // Question sent to receivers msg


     */

    function showQuestion() {
        var q = quesModules.questionSet.questions[index];
        var msg = {};
        msg.msgType = "question";
        msg.qIndex = index;
        msg.q = q;

        var messageObj = {};
        messageObj.redirectClient = "1000";
        messageObj.msg = msg;


        console.log("showQuestion() sending to receiver , count:" + numberOfClient);
        console.log(JSON.stringify(messageObj));

        $('.host #showQuestion #question').text(q.question);



        tmpNumberOfClient = numberOfClient;
        wsSendMessage(JSON.stringify(messageObj));
        index++;

        $('.host #showQuestion #qIndex').text(""+index);
        $('.host #showQuestion #qTotal').text(""+quesModules.questionSet.questions.length);

        $('.host #showQuestion #hostWaitingForAnswer').show();
        $('.host #showQuestion #hostGotAllAnswers').hide();

    }

    function restartQuiz() {

        var msg = {};
        msg.msgType = "restartQuiz";

        var messageObj = {};
        messageObj.redirectClient = "1000";
        messageObj.msg = msg;


        console.log("restartQuiz() sending to receiver ");
        console.log(JSON.stringify(messageObj));

        wsSendMessage(JSON.stringify(messageObj));

    }


    function wsOpen(message) {
        echoText.value += "Connected ... \n";
        showWaitingMsg();
    }


    function wsSendMessage(message) {
        webSocket.send(message);
        echoText.value += "Message sent to the server : " + message + "\n";
        message.value = "";
    }

    function wsGetMessage(message) {
        var msgObj = Utils.getJsonMsgObject(message.data);
        console.log("Message Received :: " + msgObj.msgType);
        console.log(msgObj);
        if (msgObj.msgType == "connection") {
            console.log(msgObj.name + " is connected !!");
            if(!started) {
                var html_element = Utils.createParticipantElement(msgObj.name);
                $("#showParticipants").append(html_element);
                $('#startButton').show();
            }
            numberOfClient++;

        } else if (msgObj.msgType == "totalResult") {

            $('.host #showQuestion').hide();
            $('.host #showResult').show();
            $('.host #showResult').css('display', 'inline-block');
            var html_element = Utils.createParticipantElementResult(msgObj.name,msgObj.totalCorrectAnswer);
            $(".host #showResult").append(html_element);






        } else if (msgObj.msgType == "answer") {
            console.log("Answer received =" + tmpNumberOfClient);
            if (tmpNumberOfClient <= 1) {
                var msg = {};
                if (index >= numberOfQuestion) {
                    msg.msgType = "sendResult";

                } else {
                    msg.msgType = "showAnswer";
                    $('.host #showQuestion #hostWaitingForAnswer').hide();
                    $('.host #showQuestion #hostGotAllAnswers').show();
                }
                var messageObj = {};
                messageObj.redirectClient = "1000";
                messageObj.msg = msg;

                // Need to show Next Button Here
                wsSendMessage(JSON.stringify(messageObj));
            }
            else {
                tmpNumberOfClient--;
            }


        }else if (msgObj.msgType == "receiverClosed") {
            console.log("receiverClosed");
            numberOfClient--;
            tmpNumberOfClient--;

        }
        echoText.value += "Message received from to the server : " + message.data + "\n";
    }

    function wsCloseConnection() {
        webSocket.close();
    }


    function wsClose(message) {
        echoText.value += "Disconnect ... \n";
    }

    function wsError(message) {
        echoText.value += "Error ... \n";
    }

    return {
        wsOpen: wsOpen,
        wsSendMessage: wsSendMessage,
        wsCloseConnection: wsCloseConnection,
        wsGetMessage: wsGetMessage,
        wsClose: wsClose,
        wsError: wsError,
        hostStart: hostStart,
        showQuestion: showQuestion,
        restartQuiz: restartQuiz

    }

};


