<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    long date = new Date().getTime();
%>
<c:if test="${empty applicationScope.quiz_created}">
    <c:set var="quiz_created" scope="application"></c:set>
</c:if>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Quiz Magic</title>

    <script
            src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="static/quiz.css?build=<%=date%>">
    <link rel="stylesheet" href="static/ws.css?build=<%=date%>">
</head>
<body class="host">
<form style="display: none;">
    <input id="message" type="text">
    <input onclick="sendMessage();" value="Echo" type="button">
    <input onclick="closeConnection();" value="Disconnect" type="button">

    <br>
    <textarea id="echoText" rows="5" cols="30"></textarea>
</form>

<div id="mainBlock">
    <div id="participantForm">
        <div id="showParticipants">
            <h1></h1>

            <h3></h3>

        </div>
        <div>
            <button id="startButton" type="button">Start</button>
        </div>
    </div>
    <div id="showQuestion" style="width: 100%" >
            <h2><span id="question"></span>( <span id="qIndex"></span>/<span id="qTotal"></span> )</h2>
            <div id="hostWaitingForAnswer" style="padding-left: 50px;"><img src="static/gfx/loading-image.gif" width="300" height="300"/></div>
            <div id="hostGotAllAnswers"><h2>Let Move to the Next Question...  </h2></div>
            <button id="nextQButton" type="button">Next</button>

    </div>
    <div id="showResult" style="width: 100%" >
        <h1>Quiz Completed</h1>
        <h2>Here are the results</h2>
    </div>


    <div id="reset" >
            <span id="restartQuiz">Restart</span>
    </div>

</div>

</body>
<script type="text/javascript">
    var QuizId = ${param.quiz_id};

</script>
<script src="static/Utils.js?build=<%=date%>"></script>
<script src="static/quiz.js?build=<%=date%>"></script>
<script src="static/host.js?build=<%=date%>"></script>
<script src="static/ws.js?build=<%=date%>"></script>
</html>